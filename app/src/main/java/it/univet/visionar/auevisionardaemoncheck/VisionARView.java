package it.univet.visionar.auevisionardaemoncheck;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import it.univet.visionar.libvarp.*;

public class VisionARView extends FrameLayout {

    static private String TAG = "AUEMSGVW";

        public VisionARView(Context context, AttributeSet attrs)  {
        super(context, attrs);
    }

    @Override
    public void dispatchDraw(Canvas canvas) {
        VisionARLog.v(TAG);

        Bitmap bitmap = Bitmap.createBitmap(this.getWidth(), this.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas newCanvas = new Canvas(bitmap);

        super.dispatchDraw(newCanvas);

        VisionARCanvas myCanvas = new VisionARCanvas();

        myCanvas.drawBitmap(bitmap);
        myCanvas.draw();
    }
}
