package it.univet.visionar.auevisionardaemoncheck;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Date;

import it.univet.visionar.libvarp.*;

public class MainActivity extends VisionARActivity {

    static private final String TAG = "AUEMSG";

    final int Get_Daemon_Info = 1;
    final int Get_Control_Unit_Info = 2;
    final int Set_Control_Unit_Batteries_Status = 3;
    final int Get_Control_Unit_Batteries_Status = 4;
    final int Set_Control_Unit_Glasses_Enable = 5;
    final int Get_Control_Unit_Glasses_Enable = 6;
    final int Set_Control_Unit_LEDs_Status = 7;
    final int Get_Control_Unit_LEDs_Status = 8;
    final int Get_Spectacles_Connection_Status = 9;
    final int Get_Spectacles_Info = 10;
    final int Get_Spectacles_Status = 11;
    final int Get_Spectacles_Magnetometer_Data = 12;
    final int Get_Spectacles_Gyroscope_Data = 13;
    final int Get_Spectacles_Compass_Data = 14;
    final int Get_Spectacles_Accelerometer_Data = 15;
    final int Set_Spectacles_Haptic = 16;
    final int Set_Spectacles_LEDs_Status = 17;
    final int Get_Spectacles_LEDs_Status = 18;
    final int Set_Spectacles_Brightness = 19;
    final int Get_Spectacles_Brightness = 20;
    final int Set_Spectacles_Contrast = 21;
    final int Get_Spectacles_Contrast = 22;
    final int Get_Spectacles_Ambient_Sensor_Light = 23;
    final int Set_Spectacles_I2C_data = 24;
    final int Get_Spectacles_I2C_Data = 25;
    final int Set_Spectacles_Display_Refresh_Time = 26;
    final int Get_Spectacles_Display_Refresh_Time = 27;
    final int Set_Spectacles_Display_Lock_Status = 28;
    final int Get_Spectacles_Display_Lock_Status = 29;
    final int Get_Camera_Info = 30;
    final int Get_Camera_Image_Formats_List = 31;
    final int Set_Camera_Current_Image_Format = 32;
    final int Get_Camera_Current_Image_Format = 33;
    final int Get_Camera_Capture = 34;
    final int Set_Camera_IO_Streaming_Enable = 35;
    final int Get_Camera_IO_Streaming_Enable = 36;
    final int Set_Camera_LED_Status = 37;
    final int Get_Camera_LED_Status = 38;
    final int Set_Camera_Brightness = 39;
    final int Get_Camera_Brightness = 40;
    final int Set_Camera_Contrast = 41;
    final int Get_Camera_Contrast = 42;

    final int MAX_INDEX = 42;

    private ViewGroup mLayoutView;
    private TextView mTitle;
    private TextView mPage;
    private TextView mBody;
    private ImageView mImage;

    private int index = 0;
    private boolean streamingStarted = false;
    private Date imageDate = null;
    private boolean lock = false;


    private class VisionARClientTaskGetDaemonInfo extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Daemon Info Response:" + result.getResponse());

            VisionARInputGetDaemonInfoResponseMessage response = new VisionARInputGetDaemonInfoResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String title = "DAEMON INFO";
                String page = index + "/" + MAX_INDEX;
                String body = "Version: " + response.getVerMajor() + "." + response.getVerMinor() + "\n"
                                + "Build Date: " + response.getBuildDay() + "/"
                                + String.format("%02d", response.getBuildMonth()) + "/"
                                + String.format("20%02d", response.getBuildYear());

                mTitle.setText(title);
                mPage.setText(page);
                mBody.setText(body);

                mTitle.setVisibility(View.VISIBLE);
                mPage.setVisibility(View.VISIBLE);
                mBody.setVisibility(View.VISIBLE);
                mImage.setVisibility(View.INVISIBLE);

                mLayoutView.invalidate();
            }
        }
    }

    private class VisionARClientTaskGetCUInfo extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Control Unit Info Response:" + result.getResponse());

            VisionARInputGetCUInfoResponseMessage response = new VisionARInputGetCUInfoResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String title = "CONTROL UNIT INFO";
                String page = index + "/" + MAX_INDEX;
                String body = "Board: " + response.getBoardName() + "\n"
                                + "Android: " + response.getAndroidMajorVersion() + "." + response.getAndroidMinorVersion() + "." + response.getAndroidPatchVersion() + " (" + response.getAndroidSdk() + ")\n"
                                + "kernel: " + response.getKernelReleaseVersion() + "." + response.getKernelMajorVersion() + "." + response.getKernelMinorVersion() + "\n"
                                + "FB: " + response.getFbDriverMajorVersion() + "." + response.getFbDriverMinorVersion() + "    "  + "USB: " + response.getUsbDriverMajorVersion() + "." + response.getUsbDriverMinorVersion();

                mTitle.setText(title);
                mPage.setText(page);
                mBody.setText(body);

                mTitle.setVisibility(View.VISIBLE);
                mPage.setVisibility(View.VISIBLE);
                mBody.setVisibility(View.VISIBLE);
                mImage.setVisibility(View.INVISIBLE);

                mLayoutView.invalidate();
            }
        }
    }

    private class VisionARClientTaskGetCUBattery extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Control Unit Battery Status Response:" + result.getResponse());

            VisionARInputGetCUBatteryStatusResponseMessage response = new VisionARInputGetCUBatteryStatusResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String title = "BATTERIES STATUS";
                String page = index + "/" + MAX_INDEX;
                String body = "Internal: " + response.getInternalBatteryLevel() + "%" + "\n"
                                + "External: " + response.getExternalBatteryLevel() + "%" + "\n"
                                + "Local: " + ((response.getLocalBatteryStatus()==true)?"TRUE":"FALSE");

                mTitle.setText(title);
                mPage.setText(page);
                mBody.setText(body);

                mTitle.setVisibility(View.VISIBLE);
                mPage.setVisibility(View.VISIBLE);
                mBody.setVisibility(View.VISIBLE);
                mImage.setVisibility(View.INVISIBLE);

                mLayoutView.invalidate();
            }
        }
    }

    private class VisionARClientTaskGetGlassesEnable extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Control Unit Glasses Enable Response:" + result.getResponse());

            VisionARInputGetCUGlassesEnableResponseMessage response = new VisionARInputGetCUGlassesEnableResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String title = "GLASSES STATUS";
                String page = index + "/" + MAX_INDEX;
                String body = (response.getEnable()?"ENABLED":"DISABLED");

                mTitle.setText(title);
                mPage.setText(page);
                mBody.setText(body);

                mTitle.setVisibility(View.VISIBLE);
                mPage.setVisibility(View.VISIBLE);
                mBody.setVisibility(View.VISIBLE);
                mImage.setVisibility(View.INVISIBLE);

                mLayoutView.invalidate();
            }
        }
    }

    private class VisionARClientTaskGetCULeds extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Control Unit Battery Status Response:" + result.getResponse());

            VisionARInputGetCULedsStatusResponseMessage response = new VisionARInputGetCULedsStatusResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String title = "CONTROL UNIT LEDS";
                String page = index + "/" + MAX_INDEX;
                String body = "GSM: " + response.getGsmLed() + "\t"
                                + "Wifi: " + response.getWifiLed() + "\n"
                                + "BT: " + response.getBtLed() + "\t"
                                + "Status: " + response.getStatusLed();

                mTitle.setText(title);
                mPage.setText(page);
                mBody.setText(body);

                mTitle.setVisibility(View.VISIBLE);
                mPage.setVisibility(View.VISIBLE);
                mBody.setVisibility(View.VISIBLE);
                mImage.setVisibility(View.INVISIBLE);

                mLayoutView.invalidate();
            }
        }
    }

    private class VisionARClientTaskGetConnStatus extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Spectacles Connection Status Response:" + result.getResponse());

            VisionARInputGetConnStatusResponseMessage response = new VisionARInputGetConnStatusResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String title = "CONNECTION STATUS";
                String page = index + "/" + MAX_INDEX;
                String body = "Spect : " + (response.getSpectaclesStatus()?"connected":"disconnected") + "\n"
                                + "Camera: " + (response.getCameraStatus()?"connected":"disconnected");

                mTitle.setText(title);
                mPage.setText(page);
                mBody.setText(body);

                mTitle.setVisibility(View.VISIBLE);
                mPage.setVisibility(View.VISIBLE);
                mBody.setVisibility(View.VISIBLE);
                mImage.setVisibility(View.INVISIBLE);

                mLayoutView.invalidate();
            }
        }
    }

    private class VisionARClientTaskGetSpectInfo extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Spectacles Info Response:" + result.getResponse());

            VisionARInputGetVersionResponseMessage response = new VisionARInputGetVersionResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String title = "SPECTACLES INFO";
                String page = index + "/" + MAX_INDEX;
                String body = "Protocol: " + response.getProtocolMajorVersion() + "." + response.getProtocolMinorVersion() + "\n"
                                + "Boot: " + response.getBootMajorVersion() + "." + response.getBootMinorVersion() + "\n"
                                + "Appl: " + response.getApplicativeMajorVersion() + "." + response.getApplicativeMinorVersion() + "." + response.getApplicativePatchVersion()
                                + " (" + response.getApplicativeDay() + "/" + response.getApplicativeMonth() + "/" + response.getApplicativeYear() + ")\n"
                                + "Type: " + response.getSpectaclesType() + "    "
                                + "Rev.: " + response.getHwRevision();

                mTitle.setText(title);
                mPage.setText(page);
                mBody.setText(body);

                mTitle.setVisibility(View.VISIBLE);
                mPage.setVisibility(View.VISIBLE);
                mBody.setVisibility(View.VISIBLE);
                mImage.setVisibility(View.INVISIBLE);

                mLayoutView.invalidate();
            }
        }
    }

    private class VisionARClientTaskGetSpectStatus extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Spectacles Info Response:" + result.getResponse());

            VisionARInputGetStatusResponseMessage response = new VisionARInputGetStatusResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String title = "SPECTACLES STATUS";
                String page = index + "/" + MAX_INDEX;
                String body = "\nStatus: " + String.format("0x%08X", response.get());

                mTitle.setText(title);
                mPage.setText(page);
                mBody.setText(body);

                mTitle.setVisibility(View.VISIBLE);
                mPage.setVisibility(View.VISIBLE);
                mBody.setVisibility(View.VISIBLE);
                mImage.setVisibility(View.INVISIBLE);

                mLayoutView.invalidate();
            }
        }
    }

    private class VisionARClientTaskGetMagnetometer extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Spectacles Magnetometer Data Response:" + result.getResponse());

            VisionARInputGetMagnetometerResponseMessage response = new VisionARInputGetMagnetometerResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String title = "MAGNETOMETER";
                String page = index + "/" + MAX_INDEX;
                String body = "X: " + response.getMagnetometerX() + "\n"
                                + "Y: " + response.getMagnetometerY() + "\n"
                                + "Z: " + response.getMagnetometerZ();

                mTitle.setText(title);
                mPage.setText(page);
                mBody.setText(body);

                mTitle.setVisibility(View.VISIBLE);
                mPage.setVisibility(View.VISIBLE);
                mBody.setVisibility(View.VISIBLE);
                mImage.setVisibility(View.INVISIBLE);

                mLayoutView.invalidate();
            }
        }
    }

    private class VisionARClientTaskGetGyroscope extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Spectacles Gyroscope Data Response:" + result.getResponse());

            VisionARInputGetGyroscopeResponseMessage response = new VisionARInputGetGyroscopeResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String title = "GYROSCOPE";
                String page = index + "/" + MAX_INDEX;
                String body = "X: " + response.getGyroscopeX() + "\n"
                        + "Y: " + response.getGyroscopeY() + "\n"
                        + "Z: " + response.getGyroscopeZ();

                mTitle.setText(title);
                mPage.setText(page);
                mBody.setText(body);

                mTitle.setVisibility(View.VISIBLE);
                mPage.setVisibility(View.VISIBLE);
                mBody.setVisibility(View.VISIBLE);
                mImage.setVisibility(View.INVISIBLE);

                mLayoutView.invalidate();
            }
        }
    }

    private class VisionARClientTaskGetCompass extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Spectacles Compass Data Response:" + result.getResponse());

            VisionARInputGetCompassResponseMessage response = new VisionARInputGetCompassResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String title = "COMPASS";
                String page = index + "/" + MAX_INDEX;
                String body = "" + response.get();

                mTitle.setText(title);
                mPage.setText(page);
                mBody.setText(body);

                mTitle.setVisibility(View.VISIBLE);
                mPage.setVisibility(View.VISIBLE);
                mBody.setVisibility(View.VISIBLE);
                mImage.setVisibility(View.INVISIBLE);

                mLayoutView.invalidate();
            }
        }
    }

    private class VisionARClientTaskGetAccelerometer extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Spectacles Accelerometer Data Response:" + result.getResponse());

            VisionARInputGetAccelerometerResponseMessage response = new VisionARInputGetAccelerometerResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String title = "ACCELEROMETER";
                String page = index + "/" + MAX_INDEX;
                String body = "X: " + response.getAccelerometerX() + "\n"
                        + "Y: " + response.getAccelerometerY() + "\n"
                        + "Z: " + response.getAccelerometerZ();

                mTitle.setText(title);
                mPage.setText(page);
                mBody.setText(body);

                mTitle.setVisibility(View.VISIBLE);
                mPage.setVisibility(View.VISIBLE);
                mBody.setVisibility(View.VISIBLE);
                mImage.setVisibility(View.INVISIBLE);

                mLayoutView.invalidate();
            }
        }
    }

    private class VisionARClientTaskGetLedsStatus extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Spectacles LEDs Status Response:" + result.getResponse());

            VisionARInputGetLedsStatusResponseMessage response = new VisionARInputGetLedsStatusResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String title = "LEDS";
                String page = index + "/" + MAX_INDEX;
                String body = "red: " + response.getRedLed() + "\n"
                        + "green: " + response.getGreenLed() + "\n"
                        + "blue: " + response.getBlueLed();

                mTitle.setText(title);
                mPage.setText(page);
                mBody.setText(body);

                mTitle.setVisibility(View.VISIBLE);
                mPage.setVisibility(View.VISIBLE);
                mBody.setVisibility(View.VISIBLE);
                mImage.setVisibility(View.INVISIBLE);

                mLayoutView.invalidate();
            }
        }
    }

    private class VisionARClientTaskGetBrightness extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Spectacles Brightness Response:" + result.getResponse());

            VisionARInputGetBrightnessResponseMessage response = new VisionARInputGetBrightnessResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String title = "BRIGHTNESS";
                String page = index + "/" + MAX_INDEX;
                String body = "" + response.get() ;

                mTitle.setText(title);
                mPage.setText(page);
                mBody.setText(body);

                mTitle.setVisibility(View.VISIBLE);
                mPage.setVisibility(View.VISIBLE);
                mBody.setVisibility(View.VISIBLE);
                mImage.setVisibility(View.INVISIBLE);

                mLayoutView.invalidate();
            }
        }
    }

    private class VisionARClientTaskGetContrast extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Spectacles Contrast Response:" + result.getResponse());

            VisionARInputGetContrastResponseMessage response = new VisionARInputGetContrastResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String title = "CONTRAST";
                String page = index + "/" + MAX_INDEX;
                String body = "" + response.get() ;

                mTitle.setText(title);
                mPage.setText(page);
                mBody.setText(body);

                mTitle.setVisibility(View.VISIBLE);
                mPage.setVisibility(View.VISIBLE);
                mBody.setVisibility(View.VISIBLE);
                mImage.setVisibility(View.INVISIBLE);

                mLayoutView.invalidate();
            }
        }
    }

    private class VisionARClientTaskGetAsl extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Spectacles Ambient Sensor Light Response:" + result.getResponse());

            VisionARInputGetAslResponseMessage response = new VisionARInputGetAslResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String title = "ASL";
                String page = index + "/" + MAX_INDEX;
                String body = "" + response.get() ;

                mTitle.setText(title);
                mPage.setText(page);
                mBody.setText(body);

                mTitle.setVisibility(View.VISIBLE);
                mPage.setVisibility(View.VISIBLE);
                mBody.setVisibility(View.VISIBLE);
                mImage.setVisibility(View.INVISIBLE);

                mLayoutView.invalidate();
            }
        }
    }

    private class VisionARClientTaskGetI2CData extends VisionARClientTask {
        private byte address;
        private short register;
        private int size;

        public VisionARClientTaskGetI2CData(byte address, short reg, int size) {
            this.address = address;
            this.register = reg;
            this.size = size;
        }

        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Spectacles I2C Data Response:" + result.getResponse());

            VisionARInputGetI2CDataResponseMessage response = new VisionARInputGetI2CDataResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String title = "I2C Data";
                String page = index + "/" + MAX_INDEX;
                String body = "Address: " + String.format("0x%02X", this.address) + "\n"
                                + "Register: " + String.format("0x%04X", this.register) + "\n";
                if (this.size == 2) {
                    body += "Value: " + String.format("0x%04X", response.get());
                }
                else {
                    body += "Value: " + String.format("0x%02X", response.get());
                }

                mTitle.setText(title);
                mPage.setText(page);
                mBody.setText(body);

                mTitle.setVisibility(View.VISIBLE);
                mPage.setVisibility(View.VISIBLE);
                mBody.setVisibility(View.VISIBLE);
                mImage.setVisibility(View.INVISIBLE);

                mLayoutView.invalidate();
            }
        }
    }

    private class VisionARClientTaskGetDisplayRefresh extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Spectacles Display Refresh Response:" + result.getResponse());

            VisionARInputGetDisplayRefreshResponseMessage response = new VisionARInputGetDisplayRefreshResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String title = "DISPLAY REFRESH TIME";
                String page = index + "/" + MAX_INDEX;
                String body = "" + response.get() ;

                mTitle.setText(title);
                mPage.setText(page);
                mBody.setText(body);

                mTitle.setVisibility(View.VISIBLE);
                mPage.setVisibility(View.VISIBLE);
                mBody.setVisibility(View.VISIBLE);
                mImage.setVisibility(View.INVISIBLE);

                mLayoutView.invalidate();
            }
        }
    }

    private class VisionARClientTaskGetDisplayLock extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Spectacles Display Lock Status Response:" + result.getResponse());

            VisionARInputGetDisplayLockStatusResponseMessage response = new VisionARInputGetDisplayLockStatusResponseMessage(result);
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String title = "DISPLAY LOCK STATUS";
                String page = index + "/" + MAX_INDEX;
                String body = "" + response.get() ;

                mTitle.setText(title);
                mPage.setText(page);
                mBody.setText(body);

                mTitle.setVisibility(View.VISIBLE);
                mPage.setVisibility(View.VISIBLE);
                mBody.setVisibility(View.VISIBLE);
                mImage.setVisibility(View.INVISIBLE);

                mLayoutView.invalidate();
            }
        }
    }

    private class VisionARClientTaskGetCameraInfo extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Camera Info Response:" + result.getResponse());

            VisionARInputGetCameraInfoResponseMessage response = new VisionARInputGetCameraInfoResponseMessage(result);
            String title = "CAMERA INFO";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String body = "Name: " + response.getName() +"\n"
                                + "Version: " + response.getMajorRelease() + "." + response.getMinorRelease() + "." + response.getPatchNumber();

                mBody.setText(body);
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskGetCameraImageFormatList extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Camera Images Format List Response:" + result.getResponse());

            VisionARInputGetCameraImageFormatListResponseMessage response = new VisionARInputGetCameraImageFormatListResponseMessage(result);
            String title = "CAMERA IMAGE LIST";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String body = "Number: " + response.getNumberOfFormats() +"\n"
                        + "Format " + response.getIdByIndex(0) + ": " + String.format("0x%08X", response.getFormatByIndex(0))
                            + " (" + response.getWidthByIndex(0) + "x" + response.getHeightByIndex(0) + ")";

                mBody.setText(body);
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskGetCameraCurrentImageFormat extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Camera Current Image Format Response:" + result.getResponse());

            VisionARInputGetCameraCurrentImageFormatResponseMessage response = new VisionARInputGetCameraCurrentImageFormatResponseMessage(result);
            String title = "CAMERA CURRENT IMAGE FORMAT";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String body = "Id: " + response.getId() + "\n"
                            + "Format: " + String.format("0x%08X", response.getFormat()) + "\n"
                            + "Size: " + response.getWidth() + "x" + response.getHeight();

                mBody.setText(body);
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskGetCameraIOStreamingEnable extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Camera I/O Streaming Enable Response:" + result.getResponse());

            VisionARInputGetCameraIOStreamingEnableResponseMessage response = new VisionARInputGetCameraIOStreamingEnableResponseMessage(result);

            String title = "CAMERA STREAMING";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String body = (response.getEnable()?"ENABLED":"DISABLED");

                mBody.setText(body);
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskGetCameraCapture extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Camera Capture Response:" + result.getResponse());

            VisionARInputGetCameraCaptureResponseMessage response = new VisionARInputGetCameraCaptureResponseMessage(result);

            String title = "CAMERA CAPTURE";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                mImage.setImageBitmap(response.getBitmap());

                mBody.setVisibility(View.INVISIBLE);
                mImage.setVisibility(View.VISIBLE);
            }
            else {
                mBody.setText("\nERROR");

                mBody.setVisibility(View.VISIBLE);
                mImage.setVisibility(View.INVISIBLE);
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskGetCameraLedStatus extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Camera LED Status Response:" + result.getResponse());

            VisionARInputGetCameraLedStatusResponseMessage response = new VisionARInputGetCameraLedStatusResponseMessage(result);
            String title = "CAMERA LED";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String body = ((response.get() == VisionARInputGetCameraLedStatusResponseMessage.CAMERA_LED_OFF)?
                        "\nOff":
                        ((response.get() == VisionARInputGetCameraLedStatusResponseMessage.CAMERA_LED_RED)?"\nRed":"\nGreen"));

                mBody.setText(body);
            }
            else {
                mBody.setText("\nERROR");
            }

            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskGetCameraBrightness extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Camera Brightness Response:" + result.getResponse());

            VisionARInputGetCameraBrightnessResponseMessage response = new VisionARInputGetCameraBrightnessResponseMessage(result);

            String title = "CAMERA BRIGTHNESS";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String body = "Brightness: " + response.get();

                mBody.setText(body);
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskGetCameraContrast extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Get Camera Contrast Response:" + result.getResponse());

            VisionARInputGetCameraContrastResponseMessage response = new VisionARInputGetCameraContrastResponseMessage(result);

            String title = "CAMERA CONTRAST";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                String body = "Contrast: " + response.get();

                mBody.setText(body);
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskSetCUBatteriesStatus extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Set Control Unit Batteries Status Response:" + result.getResponse());

            VisionARInputSetResponseMessage response = new VisionARInputSetResponseMessage(VisionARMessage.SET_CU_BATT_STATUS, result);

            String title = "BATTERIES STATUS";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                mBody.setText("\nOK");
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskSetCUGlassesEnable extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Set Control Unit Glasses Enable Response:" + result.getResponse());

            VisionARInputSetResponseMessage response = new VisionARInputSetResponseMessage(VisionARMessage.SET_CU_GLASS_ENABLE, result);

            String title = "GLASSES ENABLE";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                mBody.setText("\nOK");
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskSetCULedsStatus extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Set Control Unit LEDs Status Response:" + result.getResponse());

            VisionARInputSetResponseMessage response = new VisionARInputSetResponseMessage(VisionARMessage.SET_CU_LEDS_STATUS, result);

            String title = "CU LEDS STATUS";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                mBody.setText("\nOK");
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskSetHaptic extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Set Haptic Response:" + result.getResponse());

            VisionARInputSetResponseMessage response = new VisionARInputSetResponseMessage(VisionARMessage.SET_HAPTIC, result);

            String title = "SPECTACLES HAPTIC";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                mBody.setText("\nOK");
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskSetDisplayImage extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Set Spectacles Display Image Response:" + result.getResponse());

            VisionARInputSetResponseMessage response = new VisionARInputSetResponseMessage(VisionARMessage.SET_FB, result);

            String title = "SPECTACLES DISPLAY IMAGE";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                mBody.setText("\nOK");
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskSetLedsStatus extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Set LEDs Status Response:" + result.getResponse());

            VisionARInputSetResponseMessage response = new VisionARInputSetResponseMessage(VisionARMessage.SET_LEDS_STATUS, result);

            String title = "SPECTACLES LEDS";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                mBody.setText("\nOK");
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskSetBrightness extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Set Brightness Response:" + result.getResponse());

            VisionARInputSetResponseMessage response = new VisionARInputSetResponseMessage(VisionARMessage.SET_BRIGHTNESS, result);

            String title = "SPECTACLES BRIGHTNESS";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                mBody.setText("\nOK");
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskSetContrast extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Set Contrast Response:" + result.getResponse());

            VisionARInputSetResponseMessage response = new VisionARInputSetResponseMessage(VisionARMessage.SET_CONTRAST, result);

            String title = "SPECTACLES CONTRAST";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                mBody.setText("\nOK");
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskSetI2CData extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Set I2C Data Response:" + result.getResponse());

            VisionARInputSetResponseMessage response = new VisionARInputSetResponseMessage(VisionARMessage.SET_I2C_DATA, result);

            String title = "I2C DATA";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                mBody.setText("\nOK");
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskSetDisplayRefresh extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Set Display Refresh Time Response:" + result.getResponse());

            VisionARInputSetResponseMessage response = new VisionARInputSetResponseMessage(VisionARMessage.SET_LCD_REFRESH, result);

            String title = "SPECTACLES DISPLAY REFRESH";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                mBody.setText("\nOK");
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskSetDisplayLock extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Set Display Refresh Lock Status Response:" + result.getResponse());

            VisionARInputSetResponseMessage response = new VisionARInputSetResponseMessage(VisionARMessage.SET_LOCK_STATUS, result);

            String title = "SPECTACLES LOCK STATUS";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                mBody.setText("\nOK");
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskSetCameraImageFormat extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Set Camera Current Image Format Response:" + result.getResponse());

            VisionARInputSetResponseMessage response = new VisionARInputSetResponseMessage(VisionARMessage.SET_CAM_IMG, result);

            String title = "CAMERA IMAGE FORMAT";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                mBody.setText("\nOK");
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskSetCameraIOStreamingEnable extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Set Camera I/O Streaming Enable Response:" + result.getResponse());

            VisionARInputSetResponseMessage response = new VisionARInputSetResponseMessage(VisionARMessage.SET_CAM_STREAM_ENABLE, result);

            String title = "CAMERA STREAMING";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                mBody.setText("\nOK");
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskSetCameraLedStatus extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Set Camera LED Status Response:" + result.getResponse());

            VisionARInputSetResponseMessage response = new VisionARInputSetResponseMessage(VisionARMessage.SET_CAM_LED_STATUS, result);

            String title = "CAMERA LED";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                mBody.setText("\nOK");
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskSetCameraBrightness extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Set Camera Brightness Response:" + result.getResponse());

            VisionARInputSetResponseMessage response = new VisionARInputSetResponseMessage(VisionARMessage.SET_CAM_BRIGHTNESS, result);

            String title = "CAMERA BRIGHTNESS";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                mBody.setText("\nOK");
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }

    private class VisionARClientTaskSetCameraContrast extends VisionARClientTask {
        @Override
        protected void onPostExecute(VisionARInputMessage result) {
            VisionARLog.v(TAG, "Set Camera Contrast Response:" + result.getResponse());

            VisionARInputSetResponseMessage response = new VisionARInputSetResponseMessage(VisionARMessage.SET_CAM_CONTRAST, result);

            String title = "CAMERA CONTRAST";
            String page = index + "/" + MAX_INDEX;
            if (response.getResponse() == VisionARMessage.OK_RESPONSE) {
                mBody.setText("\nOK");
            }
            else {
                mBody.setText("\nERROR");
            }
            mTitle.setText(title);
            mPage.setText(page);

            mTitle.setVisibility(View.VISIBLE);
            mPage.setVisibility(View.VISIBLE);
            mBody.setVisibility(View.VISIBLE);
            mImage.setVisibility(View.INVISIBLE);

            mLayoutView.invalidate();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        VisionARLog.d(TAG, "STARTED!");

        initView();

        vibration(100);
        try {
            Thread.sleep(200);
        } catch (Exception e) {
        }
        vibration(100);
}

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onControlUnitKeyReceived(VisionARInputKeyNotification msg) {
        VisionARLog.i(TAG, "code = " + msg.getCode() + ", value = " + msg.getValue());

        int oldIndex = index;
        boolean enter = false;

        if (msg.getValue() == 1) {
            switch (msg.getCode()) {
                case VisionARMessage.ENTER_BUTTON_KEY:
                    switch (index) {
                        case Set_Control_Unit_Batteries_Status:
                        case Set_Control_Unit_Glasses_Enable:
                        case Set_Control_Unit_LEDs_Status:
                        case Set_Spectacles_Haptic:
                        case Set_Spectacles_LEDs_Status:
                        case Set_Spectacles_Brightness:
                        case Set_Spectacles_Contrast:
                        case Set_Spectacles_I2C_data:
                        case Set_Spectacles_Display_Refresh_Time:
                        case Set_Spectacles_Display_Lock_Status:
                        case Set_Camera_Current_Image_Format:
                        case Set_Camera_IO_Streaming_Enable:
                        case Set_Camera_LED_Status:
                        case Set_Camera_Brightness:
                        case Set_Camera_Contrast:
                            enter = true;
                            break;
                        default:
                            index = 1;
                            break;
                    }
                    break;
                case VisionARMessage.UP_BUTTON_KEY:
                    index++;
                    break;
                case VisionARMessage.DOWN_BUTTON_KEY:
                    index--;
                    break;

                default:
                    break;
            }
        } // if (msg.getValue() == 1)

        if (index < 1) index = MAX_INDEX;
        if (index > MAX_INDEX) index = 1;

        if (enter || (index != oldIndex)) {
            switch (index) {
                case Get_Daemon_Info:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_PROT_INFO);
                        VisionARClientTaskGetDaemonInfo task = new VisionARClientTaskGetDaemonInfo();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Control_Unit_Info:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_CU_INFO);
                        VisionARClientTaskGetCUInfo task = new VisionARClientTaskGetCUInfo();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Control_Unit_Batteries_Status:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_CU_BATT_STATUS);
                        VisionARClientTaskGetCUBattery task = new VisionARClientTaskGetCUBattery();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Control_Unit_Glasses_Enable:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_CU_GLASS_ENABLE);
                        VisionARClientTaskGetGlassesEnable task = new VisionARClientTaskGetGlassesEnable();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Control_Unit_LEDs_Status:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_CU_LEDS_STATUS);
                        VisionARClientTaskGetCULeds task = new VisionARClientTaskGetCULeds();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Spectacles_Connection_Status:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_CONN_STATUS);
                        VisionARClientTaskGetConnStatus task = new VisionARClientTaskGetConnStatus();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Spectacles_Info:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_VERSION);
                        VisionARClientTaskGetSpectInfo task = new VisionARClientTaskGetSpectInfo();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Spectacles_Status:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_STATUS);
                        VisionARClientTaskGetSpectStatus task = new VisionARClientTaskGetSpectStatus();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Spectacles_Magnetometer_Data:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_MAG_DATA);
                        VisionARClientTaskGetMagnetometer task = new VisionARClientTaskGetMagnetometer();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Spectacles_Gyroscope_Data:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_GYR_DATA);
                        VisionARClientTaskGetGyroscope task = new VisionARClientTaskGetGyroscope();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Spectacles_Compass_Data:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_COMP_DATA);
                        VisionARClientTaskGetCompass task = new VisionARClientTaskGetCompass();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Spectacles_Accelerometer_Data:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_ACC_DATA);
                        VisionARClientTaskGetAccelerometer task = new VisionARClientTaskGetAccelerometer();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Spectacles_LEDs_Status:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_LEDS_STATUS);
                        VisionARClientTaskGetLedsStatus task = new VisionARClientTaskGetLedsStatus();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Spectacles_Brightness:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_BRIGHTNESS);
                        VisionARClientTaskGetBrightness task = new VisionARClientTaskGetBrightness();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Spectacles_Contrast:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_CONTRAST);
                        VisionARClientTaskGetContrast task = new VisionARClientTaskGetContrast();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Spectacles_Ambient_Sensor_Light:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_ASL);
                        VisionARClientTaskGetAsl task = new VisionARClientTaskGetAsl();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Spectacles_I2C_Data:
                    try {
                        final byte addr = (byte)0xC0;
                        final short index = (short)0x05;
                        final int size = 1; // 8-bit size
                        VisionAROutputGetI2CDataRequestMessage message = new VisionAROutputGetI2CDataRequestMessage(addr, index, size);
                        VisionARClientTaskGetI2CData task = new VisionARClientTaskGetI2CData(addr, index, size);
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Spectacles_Display_Refresh_Time:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_LCD_REFRESH);
                        VisionARClientTaskGetDisplayRefresh task = new VisionARClientTaskGetDisplayRefresh();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Spectacles_Display_Lock_Status:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_LOCK_STATUS);
                        VisionARClientTaskGetDisplayLock task = new VisionARClientTaskGetDisplayLock();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Camera_Info:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_CAM_INFO);
                        VisionARClientTaskGetCameraInfo task = new VisionARClientTaskGetCameraInfo();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Camera_Image_Formats_List:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_CAM_IMG_LIST);
                        VisionARClientTaskGetCameraImageFormatList task = new VisionARClientTaskGetCameraImageFormatList();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Camera_Current_Image_Format:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_CAM_IMG);
                        VisionARClientTaskGetCameraCurrentImageFormat task = new VisionARClientTaskGetCameraCurrentImageFormat();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Camera_IO_Streaming_Enable:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_CAM_STREAM_ENABLE);
                        VisionARClientTaskGetCameraIOStreamingEnable task = new VisionARClientTaskGetCameraIOStreamingEnable();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Camera_Capture:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_CAM_CAPTURE);
                        VisionARClientTaskGetCameraCapture task = new VisionARClientTaskGetCameraCapture();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Camera_LED_Status:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_CAM_LED_STATUS);
                        VisionARClientTaskGetCameraLedStatus task = new VisionARClientTaskGetCameraLedStatus();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Camera_Brightness:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_CAM_BRIGHTNESS);
                        VisionARClientTaskGetCameraBrightness task = new VisionARClientTaskGetCameraBrightness();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;
                case Get_Camera_Contrast:
                    try {
                        VisionAROutputGetRequestMessage message = new VisionAROutputGetRequestMessage(VisionARMessage.GET_CAM_CONTRAST);
                        VisionARClientTaskGetCameraContrast task = new VisionARClientTaskGetCameraContrast();
                        task.execute( new VisionARClientTaskParams(message));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    break;

                case Set_Control_Unit_Batteries_Status:
                    if (enter == false) {
                        String title = "BATTERIES STATUS";
                        String page = index + "/" + MAX_INDEX;
                        String body = "Press enter button to send" + "\n" +
                                "a Set Control Unit Batteries Status message";

                        mBody.setText(body);
                        mTitle.setText(title);
                        mPage.setText(page);

                        mTitle.setVisibility(View.VISIBLE);
                        mPage.setVisibility(View.VISIBLE);
                        mBody.setVisibility(View.VISIBLE);
                        mImage.setVisibility(View.INVISIBLE);

                        mLayoutView.invalidate();
                    }
                    else {
                        try {
                            VisionAROutputSetCUBatteriesStatusMessage message = new VisionAROutputSetCUBatteriesStatusMessage(true);
                            VisionARClientTaskSetCUBatteriesStatus task = new VisionARClientTaskSetCUBatteriesStatus();
                            task.execute(new VisionARClientTaskParams(message));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case Set_Control_Unit_Glasses_Enable:
                    if (enter == false) {
                        String title = "GLASSES ENABLE";
                        String page = index + "/" + MAX_INDEX;
                        String body = "Press enter button to send" + "\n" +
                                "a Set Control Unit Glasses Enable message";

                        mBody.setText(body);
                        mTitle.setText(title);
                        mPage.setText(page);

                        mTitle.setVisibility(View.VISIBLE);
                        mPage.setVisibility(View.VISIBLE);
                        mBody.setVisibility(View.VISIBLE);
                        mImage.setVisibility(View.INVISIBLE);

                        mLayoutView.invalidate();
                    }
                    else {
                        try {
                            VisionAROutputSetCUGlassesEnableMessage message = new VisionAROutputSetCUGlassesEnableMessage(true);
                            VisionARClientTaskSetCUGlassesEnable task = new VisionARClientTaskSetCUGlassesEnable();
                            task.execute(new VisionARClientTaskParams(message));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case Set_Control_Unit_LEDs_Status:
                    if (enter == false) {
                        String title = "CU LEDS STATUS";
                        String page = index + "/" + MAX_INDEX;
                        String body = "Press enter button to send" + "\n" +
                                "a Set Control Unit LEDs status message";

                        mBody.setText(body);
                        mTitle.setText(title);
                        mPage.setText(page);

                        mTitle.setVisibility(View.VISIBLE);
                        mPage.setVisibility(View.VISIBLE);
                        mBody.setVisibility(View.VISIBLE);
                        mImage.setVisibility(View.INVISIBLE);

                        mLayoutView.invalidate();
                    }
                    else {
                        try {
                            VisionAROutputSetCULEDsStatusMessage message = new VisionAROutputSetCULEDsStatusMessage(0, 1, 0, 4);
                            VisionARClientTaskSetCULedsStatus task = new VisionARClientTaskSetCULedsStatus();
                            task.execute(new VisionARClientTaskParams(message));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case Set_Spectacles_Haptic:
                    if (enter == false) {
                        String title = "SPECTACLES HAPTIC";
                        String page = index + "/" + MAX_INDEX;
                        String body = "Press enter button to send" + "\n" +
                                "a Set Haptic message";

                        mBody.setText(body);
                        mTitle.setText(title);
                        mPage.setText(page);

                        mTitle.setVisibility(View.VISIBLE);
                        mPage.setVisibility(View.VISIBLE);
                        mBody.setVisibility(View.VISIBLE);
                        mImage.setVisibility(View.INVISIBLE);

                        mLayoutView.invalidate();
                    }
                    else {
                        try {
                            VisionAROutputSetHapticMessage message = new VisionAROutputSetHapticMessage(100);
                            VisionARClientTaskSetHaptic task = new VisionARClientTaskSetHaptic();
                            task.execute(new VisionARClientTaskParams(message));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case Set_Spectacles_LEDs_Status:
                    if (enter == false) {
                        String title = "SPECTACLES LEDS";
                        String page = index + "/" + MAX_INDEX;
                        String body = "Press enter button to send" + "\n" +
                                "a Set Spectacles LED status message";

                        mBody.setText(body);
                        mTitle.setText(title);
                        mPage.setText(page);

                        mTitle.setVisibility(View.VISIBLE);
                        mPage.setVisibility(View.VISIBLE);
                        mBody.setVisibility(View.VISIBLE);
                        mImage.setVisibility(View.INVISIBLE);

                        mLayoutView.invalidate();
                    }
                    else {
                        try {
                            VisionAROutputSetLEDsStatusMessage message = new VisionAROutputSetLEDsStatusMessage(0, 255, 0);
                            VisionARClientTaskSetLedsStatus task = new VisionARClientTaskSetLedsStatus();
                            task.execute(new VisionARClientTaskParams(message));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case Set_Spectacles_Brightness:
                    if (enter == false) {
                        String title = "BRIGHTNESS";
                        String page = index + "/" + MAX_INDEX;
                        String body = "Press enter button to send" + "\n" +
                                "a Set Spectacles Brightness message";

                        mBody.setText(body);
                        mTitle.setText(title);
                        mPage.setText(page);

                        mTitle.setVisibility(View.VISIBLE);
                        mPage.setVisibility(View.VISIBLE);
                        mBody.setVisibility(View.VISIBLE);
                        mImage.setVisibility(View.INVISIBLE);

                        mLayoutView.invalidate();
                    }
                    else {
                        try {
                            VisionAROutputSetBrightnessMessage message = new VisionAROutputSetBrightnessMessage(200);
                            VisionARClientTaskSetBrightness task = new VisionARClientTaskSetBrightness();
                            task.execute(new VisionARClientTaskParams(message));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case Set_Spectacles_Contrast:
                    if (enter == false) {
                        String title = "CONTRAST";
                        String page = index + "/" + MAX_INDEX;
                        String body = "Press enter button to send" + "\n" +
                                "a Set Spectacles Contrast message";

                        mBody.setText(body);
                        mTitle.setText(title);
                        mPage.setText(page);

                        mTitle.setVisibility(View.VISIBLE);
                        mPage.setVisibility(View.VISIBLE);
                        mBody.setVisibility(View.VISIBLE);
                        mImage.setVisibility(View.INVISIBLE);

                        mLayoutView.invalidate();
                    }
                    else {
                        try {
                            VisionAROutputSetContrastMessage message = new VisionAROutputSetContrastMessage(100);
                            VisionARClientTaskSetContrast task = new VisionARClientTaskSetContrast();
                            task.execute(new VisionARClientTaskParams(message));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case Set_Spectacles_I2C_data:
                    if (enter == false) {
                        String title = "I2C DATA";
                        String page = index + "/" + MAX_INDEX;
                        String body = "Press enter button to send" + "\n" +
                                "a Set Spectacles I2C Data message";

                        mBody.setText(body);
                        mTitle.setText(title);
                        mPage.setText(page);

                        mTitle.setVisibility(View.VISIBLE);
                        mPage.setVisibility(View.VISIBLE);
                        mBody.setVisibility(View.VISIBLE);
                        mImage.setVisibility(View.INVISIBLE);

                        mLayoutView.invalidate();
                    }
                    else {
                        try {
                            VisionAROutputSetI2CDataMessage message = new VisionAROutputSetI2CDataMessage(0xC0, 0x05,1, 0xF1);
                            VisionARClientTaskSetI2CData task = new VisionARClientTaskSetI2CData();
                            task.execute(new VisionARClientTaskParams(message));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case Set_Spectacles_Display_Refresh_Time:
                    if (enter == false) {
                        String title = "SPECTACLES DISPLAY REFRESH";
                        String page = index + "/" + MAX_INDEX;
                        String body = "Press enter button to send" + "\n" +
                                "a Set Spectacles Display Refresh Time message";

                        mBody.setText(body);
                        mTitle.setText(title);
                        mPage.setText(page);

                        mTitle.setVisibility(View.VISIBLE);
                        mPage.setVisibility(View.VISIBLE);
                        mBody.setVisibility(View.VISIBLE);
                        mImage.setVisibility(View.INVISIBLE);

                        mLayoutView.invalidate();
                    }
                    else {
                        try {
                            VisionAROutputSetDisplayRefreshTimeMessage message = new VisionAROutputSetDisplayRefreshTimeMessage(100);
                            VisionARClientTaskSetDisplayRefresh task = new VisionARClientTaskSetDisplayRefresh();
                            task.execute(new VisionARClientTaskParams(message));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case Set_Spectacles_Display_Lock_Status:
                    if (enter == false) {
                        String title = "SPECTACLES LOCK STATUS";
                        String page = index + "/" + MAX_INDEX;
                        String body = "Press enter button to send" + "\n" +
                                "a Set Spectacles Display Lock Status message";

                        mBody.setText(body);
                        mTitle.setText(title);
                        mPage.setText(page);

                        mTitle.setVisibility(View.VISIBLE);
                        mPage.setVisibility(View.VISIBLE);
                        mBody.setVisibility(View.VISIBLE);
                        mImage.setVisibility(View.INVISIBLE);

                        mLayoutView.invalidate();
                    }
                    else {
                        if (lock == false) lock = true;
                        else lock = false;
                        try {
                            VisionAROutputSetLockStatusMessage message = new VisionAROutputSetLockStatusMessage(lock);
                            VisionARClientTaskSetDisplayLock task = new VisionARClientTaskSetDisplayLock();
                            task.execute(new VisionARClientTaskParams(message));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case Set_Camera_Current_Image_Format:
                    if (enter == false) {
                        String title = "CAMERA IMAGE FORMAT";
                        String page = index + "/" + MAX_INDEX;
                        String body = "Press enter button to send" + "\n" +
                                "a Set Camera Current Image Format message";

                        mBody.setText(body);
                        mTitle.setText(title);
                        mPage.setText(page);

                        mTitle.setVisibility(View.VISIBLE);
                        mPage.setVisibility(View.VISIBLE);
                        mBody.setVisibility(View.VISIBLE);
                        mImage.setVisibility(View.INVISIBLE);

                        mLayoutView.invalidate();
                    }
                    else {
                        try {
                            VisionAROutputSetCameraCurrentImageFormatMessage message = new VisionAROutputSetCameraCurrentImageFormatMessage(1);
                            VisionARClientTaskSetCameraImageFormat task = new VisionARClientTaskSetCameraImageFormat();
                            task.execute(new VisionARClientTaskParams(message));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case Set_Camera_IO_Streaming_Enable:
                    if (enter == false) {
                        String title = "CAMERA STREAMING";
                        String page = index + "/" + MAX_INDEX;
                        String body = "Press enter button to send" + "\n" +
                                "a Set Camera I/O Streaming Enable message";

                        mBody.setText(body);
                        mTitle.setText(title);
                        mPage.setText(page);

                        mTitle.setVisibility(View.VISIBLE);
                        mPage.setVisibility(View.VISIBLE);
                        mBody.setVisibility(View.VISIBLE);
                        mImage.setVisibility(View.INVISIBLE);

                        mLayoutView.invalidate();
                    }
                    else {
                        try {
                            VisionAROutputSetCameraIOStreamingEnableMessage message = new VisionAROutputSetCameraIOStreamingEnableMessage(!streamingStarted);
                            streamingStarted = !streamingStarted;
                            VisionARClientTaskSetCameraIOStreamingEnable task = new VisionARClientTaskSetCameraIOStreamingEnable();
                            task.execute(new VisionARClientTaskParams(message));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case Set_Camera_LED_Status:
                    if (enter == false) {
                        String title = "CAMERA LED";
                        String page = index + "/" + MAX_INDEX;
                        String body = "Press enter button to send" + "\n" +
                                "a Set Camera LED Status message";

                        mBody.setText(body);
                        mTitle.setText(title);
                        mPage.setText(page);

                        mTitle.setVisibility(View.VISIBLE);
                        mPage.setVisibility(View.VISIBLE);
                        mBody.setVisibility(View.VISIBLE);
                        mImage.setVisibility(View.INVISIBLE);

                        mLayoutView.invalidate();
                    }
                    else {
                        try {
                            VisionAROutputSetCameraLedStatusMessage message = new VisionAROutputSetCameraLedStatusMessage(VisionARMessage.CAMERA_LED_OFF);
                            VisionARClientTaskSetCameraLedStatus task = new VisionARClientTaskSetCameraLedStatus();
                            task.execute(new VisionARClientTaskParams(message));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case Set_Camera_Brightness:
                    if (enter == false) {
                        String title = "CAMERA BRIGHTNESS";
                        String page = index + "/" + MAX_INDEX;
                        String body = "Press enter button to send" + "\n" +
                                "a Set Camera Brightness message";

                        mBody.setText(body);
                        mTitle.setText(title);
                        mPage.setText(page);

                        mTitle.setVisibility(View.VISIBLE);
                        mPage.setVisibility(View.VISIBLE);
                        mBody.setVisibility(View.VISIBLE);
                        mImage.setVisibility(View.INVISIBLE);

                        mLayoutView.invalidate();
                    }
                    else {
                        try {
                            VisionAROutputSetCameraBrightnessMessage message = new VisionAROutputSetCameraBrightnessMessage(3);
                            VisionARClientTaskSetCameraBrightness task = new VisionARClientTaskSetCameraBrightness();
                            task.execute(new VisionARClientTaskParams(message));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;
                case Set_Camera_Contrast:
                    if (enter == false) {
                        String title = "CAMERA CONTRAST";
                        String page = index + "/" + MAX_INDEX;
                        String body = "Press enter button to send" + "\n" +
                                "a Set Camera Contrast message";

                        mBody.setText(body);
                        mTitle.setText(title);
                        mPage.setText(page);

                        mTitle.setVisibility(View.VISIBLE);
                        mPage.setVisibility(View.VISIBLE);
                        mBody.setVisibility(View.VISIBLE);
                        mImage.setVisibility(View.INVISIBLE);

                        mLayoutView.invalidate();
                    }
                    else {
                        try {
                            VisionAROutputSetCameraContrastMessage message = new VisionAROutputSetCameraContrastMessage(20);
                            VisionARClientTaskSetCameraContrast task = new VisionARClientTaskSetCameraContrast();
                            task.execute(new VisionARClientTaskParams(message));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    break;

                default:
                    break;
            }
        }
    }

    protected void onSpectaclesDisplayLockChangedReceived(VisionARDisplayLockChangedNotification msg) {
        VisionARLog.i(TAG, "" + msg.getStatus());
    }

    protected void onConnectionReceived(VisionARConnectionStatusNotification msg) {
        VisionARLog.i(TAG, "" + msg.getStatus());
    }

    protected void onCameraImageReceived(VisionARCameraCapturingNotification msg) {
        VisionARLog.i(TAG, msg.getDate() + ": " + msg.getFormat() + "(" + msg.getWidth() + "x" + msg.getHeight() + ")");

        boolean skip;

        if (imageDate == null) {
            imageDate = msg.getDate();
            skip = false;
        }
        else {
            long diff = msg.getDate().getTime() - imageDate.getTime();
            if (diff < 100) {
                skip = true;
            }
            else {
                imageDate = msg.getDate();
                skip = false;
            }
        }

        if (!skip) {
            mImage.setImageBitmap(msg.getBitmap());

            mBody.setVisibility(View.INVISIBLE);
            mImage.setVisibility(View.VISIBLE);

            mLayoutView.invalidate();
        }

        VisionARLog.i(TAG, "END!!!!");
    }

    private void initView() {
        setContentView(R.layout.activity_main);

        mLayoutView = findViewById(R.id.view);
        mTitle = findViewById(R.id.title);
        mPage = findViewById(R.id.page);
        mBody = findViewById(R.id.body);
        mImage = findViewById(R.id.capture);

        mTitle.setVisibility(View.INVISIBLE);
        mPage.setVisibility(View.INVISIBLE);
        mBody.setVisibility(View.VISIBLE);
        mImage.setVisibility(View.INVISIBLE);

        mLayoutView.invalidate();
    }

    private void vibration(int ms) {
        VisionARLog.i(TAG, new Throwable().getStackTrace()[0].getMethodName() + "(" + ms + ")");

        try {
            VisionAROutputSetHapticMessage message = new VisionAROutputSetHapticMessage(ms);
            VisionARClientTask task = new VisionARClientTask();
            task.execute( new VisionARClientTaskParams(message));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
